import { useRef, useContext, useEffect, useState } from 'react'
import {
  motion,
  useMotionValue,
  useSpring,
  useTransform
} from "framer-motion"
import useResizeObserver from '@react-hook/resize-observer'
import { LayoutContext } from './layout.context'


export default function AnimatedTrackByScroll({ children, className, getTopOffset, debug }) {
  useContext(LayoutContext) // no se porqué necesita al context

  const target = useRef(null)
  const elementScrollInView = useMotionValue(0);
  const [ maxTop, setMaxTop ] = useState(0);
  const [ maxWidth, setMaxWidth ] = useState(2000);
  const margins = 15

  useResizeObserver(target, () => {
    const _maxWidth = 0
    target.current.children
    for (const index in target.current.children) {
      if (Object.hasOwnProperty.call(target.current.children, index)) {
        _maxWidth += target.current.children[index].offsetWidth
      }
    }
    _maxWidth += target.current.children.length * margins
    setMaxWidth(_maxWidth)
  })

  useEffect(() => {
    const size = target.current.getBoundingClientRect()
    if (!maxTop) {
      setMaxTop(window.innerHeight + size.height);
    }
    const _topOffset = getTopOffset ? getTopOffset() : 0
    elementScrollInView.set(window.innerHeight + size.height - size.bottom - _topOffset)
    if (debug) {
      console.log(size, size.width)
    }
  })

  const scaleYRange = useTransform(elementScrollInView, [-1, 0, 1, maxTop, maxTop+1], [0, 0, 1, maxWidth+50, maxWidth+50]);
  const scaleValue = useSpring(scaleYRange, { stiffness: 300, damping: 50 });

  return (
    <motion.div 
      ref={target} 
      className={`relative flex flex-row place-items-center flex-shrink-0 ${className}`}
      style={{
        width: maxWidth,
        right: scaleValue
      }}
    >
      {children}
    </motion.div>
  )
} 