import { useRef, useContext, useEffect, useState } from 'react'
import {
  motion,
  useMotionValue,
  useSpring,
  useTransform
} from "framer-motion"
import { LayoutContext } from './layout.context'


export default function AnimatedGliphTrackByScroll({ children, className, getTopOffset, direction='left', debug }) {
  useContext(LayoutContext) // no se porqué necesita al context

  const target = useRef(null)
  const elementScrollInView = useMotionValue(0);
  const [ maxTop, setMaxTop ] = useState(0);

  useEffect(() => {
    const size = target.current.getBoundingClientRect()
    if (!maxTop) {
      setMaxTop(window.innerHeight + size.height);
    }
    const _topOffset = getTopOffset ? getTopOffset() : 0
    elementScrollInView.set(window.innerHeight + size.height - size.bottom - _topOffset)
    if (debug) {
      console.log(size, size.width)
    }
  })
  const maxTopReduced = maxTop/6
  let positionConfig = [
    [-1, 0, 1, maxTop, maxTop+1],
    [0, 0, 1, maxTopReduced, maxTopReduced]
  ]
  if (direction === 'right') {
    positionConfig = [
      [-1, 0, 1, maxTop, maxTop+1],
      [-maxTopReduced, -maxTopReduced, -maxTopReduced, 0, 0]
    ]
  }
  let positionXRange = useTransform(elementScrollInView, positionConfig[0], positionConfig[1]);
  const positionXValue = useSpring(positionXRange, { stiffness: 300, damping: 100 });

  let styleDef = {
    width: 3000
  }

  if (direction === 'right') {
    styleDef.left = positionXValue
  } else {
    styleDef.right = positionXValue
  }

  return (
    <motion.div 
      ref={target} 
      className={`relative ${className}`}
      style={styleDef}
    >
      {children}
    </motion.div>
  )
} 