import { useRef, useContext, useEffect, useState } from 'react'
import {
  motion,
  useMotionValue,
  useSpring,
  useTransform
} from "framer-motion"
import { LayoutContext } from './layout.context'
import { bh } from '../lib/animationHelpers'


export default function AnimatedFadeInScaleFlipAndBlurByScroll({ children, contentClass, renderFront, renderBack, className, getTopOffset, debug }) {
  useContext(LayoutContext) // no se porqué necesita al context

  const target = useRef(null)
  const elementScrollInView = useMotionValue(0);
  const [ maxTop, setMaxTop ] = useState(0);


  useEffect(() => {
    const size = target.current.getBoundingClientRect()
    if (!maxTop) {
      setMaxTop(window.innerHeight + size.height);
    }
    const _topOffset = getTopOffset ? getTopOffset() : 0
    elementScrollInView.set(window.innerHeight + size.height - size.bottom - _topOffset)
    if (debug) {
      console.log(size, size.width)
    }
  })

  // traduce el recorrido en pixeles a porcentaje
  const yRangePercent = useTransform(elementScrollInView, [-1, 0, 1, maxTop, maxTop+1], [0, 0, 0, 100, 100]);
  
  const opacityYRange = useTransform(yRangePercent, [0, 5, 15, 15.01], [0, 0, 1, 1]);
  const opacityValue = useSpring(opacityYRange, { stiffness: 400, damping: 70 });

  // const scaleYRange = useTransform(yRangePercent, [0, 5, 15, 15.01], [0.7, 0.7, 1, 1]);
  // const scaleValue = useSpring(scaleYRange, { stiffness: 400, damping: 70 });

  // const slowYRange = useTransform(yRangePercent, [0, 15, 55, 100], [0, 0, 300, 450]);

  const rotateYRange = useTransform(yRangePercent, [0, 30], [135, 180]);
  // const rotateValue = useSpring(rotateYRange, { stiffness: 600, damping: 70 });

  return (
    <motion.div 
      ref={target} 
      className={`fliping-card ${className} overflow-hidden relative`}
      style={{
        height: bh(80),
        opacity: opacityValue,
        // scale: scaleValue,
        // paddingTop: slowYRange
      }}
    >
      <div className={`scene scene--card ${contentClass}`}>
        <motion.div
          className="card"
          style={{
            rotateY: rotateYRange
          }}
        >
          <div className="card__face card__face--front">
            {renderFront({ yRangePercent, useTransform, useSpring, target })}
          </div>
          <div className="card__face card__face--back">
            {renderBack({ yRangePercent, useTransform, useSpring, target })}
          </div>
        </motion.div>
      </div>
    </motion.div>
  )
} 