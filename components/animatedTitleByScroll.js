import { useRef, useContext, useEffect, useState } from 'react'
import {
  motion,
  useMotionValue,
  useSpring,
  useTransform
} from "framer-motion"
import { LayoutContext } from './layout.context'


export default function AnimatedTitleByScroll({ children, render, className, getTopOffset, debug }) {
  useContext(LayoutContext) // no se porqué necesita al context

  const target = useRef(null)
  const elementScrollInView = useMotionValue(0);
  const [ maxTop, setMaxTop ] = useState(0);


  useEffect(() => {
    const size = target.current.getBoundingClientRect()
    if (!maxTop) {
      setMaxTop(window.innerHeight + size.height);
    }
    const _topOffset = getTopOffset ? getTopOffset() : 0
    elementScrollInView.set(window.innerHeight + size.height - size.bottom - _topOffset)
    if (debug) {
      console.log(size, size.width)
    }
  })

  // traduce el recorrido en pixeles a porcentaje
  const yRangePercent = useTransform(elementScrollInView, [-1, 0, 1, maxTop, maxTop+1], [0, 0, 0, 100, 100]);
  
  const opacityYRange = useTransform(yRangePercent, [0, 20, 30, 70, 80, 100], [0, 0, 1, 1, 0, 0]);
  const opacityValue = useSpring(opacityYRange, { stiffness: 300, damping: 50 });

  const scaleYRange = useTransform(yRangePercent, [0, 20, 30, 70, 80, 100], [0.7, 0.7, 1, 1, 0.5, 0.5]);
  const scaleValue = useSpring(scaleYRange, { stiffness: 300, damping: 50 });

  return (
    <motion.div>
      <motion.div 
        ref={target} 
        className={`relative flex flex-row place-items-center flex-shrink-0 ${className}`}
        style={{
          opacity: opacityValue,
          scale: scaleValue
        }}
      >
        {render ? render({ yRangePercent, useTransform, useSpring, target }) : children}
      </motion.div>
    </motion.div>
  )
} 