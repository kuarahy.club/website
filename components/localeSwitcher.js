import React from 'react'
import { useRouter } from 'next/router'
import config from '../config'
import { LocaleContext } from '../lib/translations/locale.context'
import useTranslation from '../lib/translations/useTranslation.hook'

export default function LocaleSwitcher({ selectClassName, optionClassName, localedPathMap }) {
  const { t, replaceInPath } = useTranslation()
  const router = useRouter()
  const { locale } = React.useContext(LocaleContext)

  const handleLocaleChange = React.useCallback(
    (e) => {
      const regex = new RegExp(`^/(${config.locales.join('|')})`)
      const newLang = e.target.value

      // custom paths
      if (localedPathMap) {
        const dest = _.find(localedPathMap, { locale: newLang })
        router.push(router.pathname, dest.path)
      }
      // default paths
      else {
        // replace locale path
        let newPath = router.asPath.replace(regex, `/${newLang}`)
        // replace all static page paths
        router.push(router.pathname, replaceInPath(newPath, locale, newLang))
      }
    },
    [locale, router]
  )

  return (
    <select value={locale} onChange={handleLocaleChange} className={'locale-switcher ' + selectClassName}>
      {config.locales.map(locale => (
        <option key={locale} value={locale} className={optionClassName}>
          {t(`common.locale-select.${locale}`)}
        </option>
      ))}
    </select>
  )
}