import React from 'react'
import _ from 'lodash'
import ReactMarkdown from 'react-markdown'
import AnimatedGliphTrackByScroll from './animatedGliphTrackByScroll'
import { Element } from 'react-scroll'
import AnimatedContentInView from './animatedContentInView'

export default function HomeContent({ data, sectionRefsObj }) {


  return (
    <div className='bg-texture-lienzo'>
      {/* La Fundación */}
      {data && data.fundacionTitle ? (
        <div id="fundacion" className="relative py-16 mx-auto bg-brand-orange-5/60">
          {/* scroll step */}
          <Element name="fundacionContent"></Element>
          <div ref={sectionRefsObj.fundacionContent.ref}></div>
          <AnimatedGliphTrackByScroll direction='right'>
            <div className="w-full min-h-[60px] bg-brand-fundacion tira-sol md:mb-10"></div>
          </AnimatedGliphTrackByScroll>
          <div className="text-center px-10 py-24 pb-32 md:pt-24 md:pb-36 lg:w-2/3 lg:py-36 mx-auto">
            <h2 className='md:scale-110 lg:scale-125 md:pb-8'>
              <span className="block text-brand-fundacion -mb-3 text-xl font-thin tracking-tight">{data.fundacionTitle[0]}</span>
              <span className="block text-brand-fundacion -mb-2 text-5xl font-black tracking-tight">{data.fundacionTitle[1]}</span>
              <span className="block text-brand-green-1 pb-10 text-2xl font-thin italic tracking-tight">{data.fundacionTitle[2]}</span>
            </h2>
            <AnimatedContentInView>
              <div className=" bg-brand-orange-6/50 rounded-xl p-5">
                <h3 className=" text-brand-green-2 pb-6 text-xl font-regular font-brand-content tracking-tight leading-7 lg:text-2xl">{data.fundacionSubtitle}</h3>
                <div className=" text-brand-green-2 text-lg font-regular prose-sm lg:text-xl">
                  <ReactMarkdown>{data.fundacionContent}</ReactMarkdown>
                </div>
              </div>
            </AnimatedContentInView>
          </div>
          {/* <AnimatedGliphTrackByScroll>
            <div className="w-full min-h-[40px] bg-brand-fundacion tira-sol"></div>
          </AnimatedGliphTrackByScroll> */}
        </div>
      ) : ''}

      {/* El Club */}
      {data && data.clubTitle ? (
        <div id="club" className="relative py-16 mx-auto bg-brand-orange-6/50 ">
          {/* scroll step */}
          <Element name="clubContent"></Element>
          <div ref={sectionRefsObj.clubContent.ref}></div>
          <AnimatedGliphTrackByScroll>
            <div className="w-full min-h-[60px] bg-brand-club tira-ola md:mb-10"></div>
          </AnimatedGliphTrackByScroll>
          <div className="text-center px-10 py-24 grid grid-cols-1 md:grid-cols-2 items-center gap-3 lg:w-3/4 lg:py-36 lg:gap-10 xl:w-2/3 mx-auto">
            <h2 className='pb-10 md:pb-20 md:scale-125 lg:scale-125 xl:scale-150 xl:pb-5'>
              <span className="block text-brand-club -mb-3 text-xl font-thin tracking-tight">{data.clubTitle[0]}</span>
              <span className="block text-brand-club -mb-2 text-6xl font-black tracking-tight">{data.clubTitle[1]}</span>
              <span className="block text-brand-orange-1 text-2xl font-thin tracking-tight">{data.clubTitle[2]}</span>
            </h2>
            <AnimatedContentInView>
              <div className=" bg-brand-orange-5/40 rounded-xl p-5">
                <h3 className=" text-brand-orange-1 pb-6 text-xl font-regular font-brand-content tracking-tight leading-7 lg:text-2xl">{data.clubSubtitle}</h3>
                <div className=" text-brand-orange-2 text-lg font-regular prose-sm lg:text-xl">
                  <ReactMarkdown>{data.clubContent}</ReactMarkdown>
                </div>
              </div>
            </AnimatedContentInView>
          </div>
          {/* <AnimatedGliphTrackByScroll>
            <div className="w-full min-h-[60px] bg-brand-club tira-ola"></div>
          </AnimatedGliphTrackByScroll> */}
        </div>
      ) : ''}

      {/* La Incubadora de Proyectos */}
      {data && data.incubadoraTitle ? (
        <div id="incubadora" className="relative py-16 mx-auto bg-brand-orange-5/60">
          {/* scroll step */}
          <Element name="incubadoraContent"></Element>
          <div ref={sectionRefsObj.incubadoraContent.ref}></div>
          <AnimatedGliphTrackByScroll direction='right'>
            <div className="w-full min-h-[60px] bg-brand-incubadora tira-triangulo md:mb-10"></div>
          </AnimatedGliphTrackByScroll>
          <div className="text-center px-10 py-24 grid grid-cols-1 md:grid-cols-2 items-center gap-3 lg:w-3/4 lg:py-36 lg:gap-10 xl:w-2/3 mx-auto">
            <h2 className='pb-10 md:pb-0 md:scale-110 2xl:scale-150 md:order-last'>
              <span className="block text-brand-incubadora -mb-3 text-xl font-thin tracking-tight">{data.incubadoraTitle[0]}</span>
              <span className="block text-brand-incubadora -mb-3 text-5xl font-black tracking-tight">{data.incubadoraTitle[1]}</span>
              <span className="block text-brand-incubadora -mb-3 text-3xl font-medium tracking-tight">{data.incubadoraTitle[2]}</span>
              <span className="block text-brand-blue-1 pb-10 text-2xl font-thin tracking-tight">{data.incubadoraTitle[3]}</span>
            </h2>
            <AnimatedContentInView>
              <div className=" bg-brand-orange-6/50 rounded-xl p-5">
                <h3 className=" text-brand-blue-2 pb-6 text-xl font-regular font-brand-content tracking-tight leading-7 lg:text-2xl">{data.incubadoraSubtitle}</h3>
                <div className=" text-brand-blue-2 text-lg font-regular prose-sm lg:text-xl">
                  <ReactMarkdown>{data.incubadoraContent}</ReactMarkdown>
                </div>
              </div>
            </AnimatedContentInView>
          </div>
          {/* <AnimatedGliphTrackByScroll>
            <div className="w-full min-h-[40px] bg-brand-incubadora tira-triangulo"></div>
          </AnimatedGliphTrackByScroll> */}
        </div>
      ) : ''}

      {/* Miembros del Staff */}
      {data && data.miembros.length ? (
        <div id="staff" className="relative mx-auto bg-brand-kuarahy/70">
          {/* scroll step */}
          <Element name="staffContent"></Element>
          <div ref={sectionRefsObj.staffContent.ref}></div>
          <div className="text-center px-10 pt-20 pb-16">
            <h2 className=" text-brand-purple-2 pb-2 text-4xl font-black sm:scale-110 lg:scale-125 tracking-tight leading-8">{data.staffTitle}</h2>
            <AnimatedContentInView>
              {data.miembros.map(({name, tags}, index) => (
                <div className="py-5" key={index}>
                  <p className=" text-brand-orange-6 text-xl -mb-1 font-black">{name}</p>
                  <p className=" text-brand-blue-2 text-md my-0 font-bold font-brand-content">{tags}</p>
                </div>
              ))}
            </AnimatedContentInView>
          </div>
        </div>
      ) : ''}

      {/* Roadmap */}
      {data && data.roadmapItems.length ? (
        <div id="roadmap" className="relative mx-auto pt-16 bg-brand-orange-6/50">
          {/* scroll step */}
          <Element name="roadmapContent"></Element>
          <div ref={sectionRefsObj.roadmapContent.ref}></div>
          <AnimatedGliphTrackByScroll direction='right'>
            <div className="w-full min-h-[60px] bg-brand-kuarahy tira-triangulo"></div>
          </AnimatedGliphTrackByScroll>
          <div className="text-center px-10 pt-20 pb-16">
            <h2 className=" text-brand-kuarahy pb-2 text-4xl font-black sm:scale-110 lg:scale-125 tracking-tight leading-8">{data.roadmapTitle}</h2>
            <AnimatedContentInView>
              <p className=" text-brand-purple-2 pb-2 text-lg tracking-tight leading-snug">
                <ReactMarkdown>{data.roadmapDesc}</ReactMarkdown>
              </p>
              {data.roadmapItems.map(({title, alert, date, content}, index) => (
                (index === 0 ? (
                  <div className="py-5 pt-9" key={index}>
                    <h3 className=" text-brand-green-3 text-xl font-black font-brand-content my-0">{title}</h3>
                    {alert ? (
                      <p className=" text-brand-orange-3 text-md font-bold my-0 font-brand-content">{alert}</p>
                    ) : ''}
                    <p className=" text-brand-purple-2 text-lg font-regular my-0 font-brand-content">{date}</p>
                    <p className=" text-brand-purple-2 text-lg font-regular my-0 font-brand-content">{content}</p>
                  </div>
                ) : (
                  <div className="py-5" key={index}>
                    <p className=" text-brand-purple-2 text-lg font-bold my-0">{title}</p>
                    <p className=" text-brand-purple-2 text-sm font-regular my-0 font-brand-content">{alert}</p>
                    <p className=" text-brand-purple-2 text-sm font-regular my-0 font-brand-content">{date}</p>
                    <p className=" text-brand-purple-2 text-sm font-regular my-0 font-brand-content">{content}</p>
                  </div>
                ))
              ))}
            </AnimatedContentInView>
          </div>
        </div>
      ) : ''}
    </div>
  )
}