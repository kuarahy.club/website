import { useContext, useEffect } from 'react'
import useTranslation from '../lib/translations/useTranslation.hook'
import { LayoutContext } from './layout.context'
import ReactMarkdown from 'react-markdown'
import { animateScroll, Element } from 'react-scroll'
import {
  motion,
  useViewportScroll
} from "framer-motion"
import AnimatedTitleByScroll from './animatedTitleByScroll'
import AnimatedFadeInScaleFlipAndBlurByScroll from './animatedFadeInScaleFlipAndBlurByScroll'
import { bh } from '../lib/animationHelpers'
import EscenaHeaderCielo from './escenaHeaderCielo'
import EscenaHeaderLogo from './escenaHeaderLogo'
import EscenaHeaderTemplo from './escenaHeaderTemplo'
import EscenaHeaderPastizal from './escenaHeaderPastizal'
import EscenaHeaderVegetacion from './escenaHeaderVegetacion'
import EscenaHeaderNubesArriba from './escenaHeaderNubesArriba'
import EscenaHeaderNubesMedio from './escenaHeaderNubesMedio'
import EscenaHeaderNubesAbajo from './escenaHeaderNubesAbajo'

export default function HeaderMain({ data, sectionRefsObj }) {
  const { t, locale } = useTranslation() 
  const { setScrollOnInit } = useContext(LayoutContext)
  // Vertical scroll distance in pixels.
  const { scrollY } = useViewportScroll();

  function handleResize() {
    // refresh framer-motion
    animateScroll.scrollMore(1)
    setTimeout(() => {
      animateScroll.scrollMore(1)
    }, 250)
    setTimeout(() => {
      animateScroll.scrollMore(1)
    }, 500)
  }
  // refresh on resize
  useEffect(() => {
    if (typeof window === "undefined") { return }
    window.addEventListener('resize', handleResize)
    return () => {
      window.removeEventListener('resize', handleResize)
    }
  })
  useEffect(() => {
    handleResize()
  }, [])
  
  return (
    <header id="header" className="flex flex-col bg-brand-orange-6" style={{zIndex: '35'}}>
      {/* scroll step logoHeader */}
      <Element name="logoHeader"></Element>
      <div ref={sectionRefsObj.logoHeader.ref}></div>

      <div className='fixed' style={{height: bh(80)}}>
        <EscenaHeaderCielo></EscenaHeaderCielo>
        <EscenaHeaderLogo></EscenaHeaderLogo>
        <EscenaHeaderNubesMedio></EscenaHeaderNubesMedio>
        <div className="md:hidden">
          <EscenaHeaderNubesArriba></EscenaHeaderNubesArriba>
          <EscenaHeaderNubesAbajo></EscenaHeaderNubesAbajo>
          <EscenaHeaderVegetacion></EscenaHeaderVegetacion>
        </div>
        <EscenaHeaderTemplo></EscenaHeaderTemplo>
        <EscenaHeaderPastizal></EscenaHeaderPastizal>
      </div>
          
      <div className="relative" style={{ marginTop: bh(95), height: bh(230) }}>
        {/* lema */}
        {/* scroll step */}
        <Element name="lemaHeader"></Element>
        <div ref={sectionRefsObj.lemaHeader.ref}></div>
        <AnimatedTitleByScroll>
          <div id="lema" name="lema" className="relative mx-auto md:mt-10 lg:mt-14">
            <div className="text-center px-10 scale-90 md:scale-110">
              {data && (<>
                <h2 className=" text-brand-green-1 text-xl font-thin -mb-2 font-brand tracking-tight">{data.lema[0]}</h2>
                <h2 className=" text-brand-green-1 text-3xl font-black mb-5 font-brand tracking-tight">{data.lema[1]}</h2>
              </>)}
            </div>
          </div>
        </AnimatedTitleByScroll>
        <AnimatedTitleByScroll>
          <div className="relative mx-auto">
            <div className="text-center px-10 scale-90 md:scale-110">
              {data && (<>
                <h2 className=" text-brand-green-1 text-xl font-thin -mb-2 font-brand tracking-tight">{data.lema[2]}</h2>
                <h2 className=" text-brand-green-1 text-3xl font-black mb-5 font-brand tracking-tight">{data.lema[3]}</h2>
              </>)}
            </div>
          </div>
        </AnimatedTitleByScroll>
        <AnimatedTitleByScroll>
          <div className="relative mx-auto">
            <div className="text-center px-10 scale-90 md:scale-110">
              {data && (<>
                <h2 className=" text-brand-green-1 text-xl -mb-2 font-thin font-brand tracking-tight">{data.lema[4]}</h2>
                <h2 className=" text-brand-green-1 text-3xl font-black font-brand tracking-tight">{data.lema[5]}</h2>
                <h2 className=" text-brand-green-1 text-5xl font-black italic font-brand tracking-tight">{data.lema[6]}</h2>
              </>)}
            </div>
          </div>
        </AnimatedTitleByScroll>

        {/* alert */}
        {data && data.alertTitle ? (
          <div id="alert" className="relative pt-[350px] mx-auto">
            {/* scroll step */}
            <Element name="alertHeader"></Element>
            <div ref={sectionRefsObj.alertHeader.ref}></div>
            <AnimatedFadeInScaleFlipAndBlurByScroll
              className='w-full mx-auto flex place-content-center lg:-mt-5'
              contentClass=" w-[250px] h-[430px] md:w-[300px] md:h-[480px]"
              renderFront={() => (
                <div className="text-center w-full h-full bg-gradient-to-b to-lime-700 from-orange-600 text-white pt-20 rounded-lg">
                  <h2 className=" block text-xl font-black tracking-tight">{data && data.alertTitle}</h2>
                </div>
              )}
              renderBack={() => (
                <div className="text-center w-full h-full bg-gradient-to-b from-lime-700 to-yellow-900 text-brand-orange-6 rounded-lg overflow-hidden">
                  <img src='/assets/elements/dorso-nft-sm.jpg' className='w-full h-auto'></img>
                  <div className='p-5'>
                    <h2>
                      <span className="block text-xl font-thin -mb-2 tracking-tight leading-8">{data.alertDesc[0]}</span>
                      <span className="block text-3xl font-black -mb-1 tracking-tight leading-8">{data.alertDesc[1]}</span>
                      <span className="block text-lg px-5 font-thin tracking-tight leading-5">{data.alertDesc[2]}</span>
                    </h2>
                    <h3 className='mt-5 text-base text-orange-200 font-medium tracking-widest'>{data.alertDesc[3]}</h3>
                  </div>
                </div>
              )}
            >
            </AnimatedFadeInScaleFlipAndBlurByScroll>
          </div>
        ) : ''}

        {/* intro */}
        <AnimatedTitleByScroll>
          {/* scroll step */}
          <Element name="propositoHeader"></Element>
          <div ref={sectionRefsObj.propositoHeader.ref}></div>
          <div id="intro" className="relative pt-44 pb-0 w-auto mx-auto">
            <div className="m-auto px-10 sm:px-28 md:flex-row md:px-48 lg:w-3/4 lg:text-xl xl:w-3/5 xl:text-2xl lg:leading-5 xl:leading-7">
              <div className="px-10 pt-16 pb-12 text-xl leading-5 text-center mx-auto prose-sm shadow-xl bg-brand-kuarahy text-brand-orange-6 lg:prose lg:scale-110">
                <ReactMarkdown components={{
                  a: ({ children, href }) => {
                    return (
                      <a href={href} target="_blank" rel="noreferrer">{children}</a>
                    )
                  }
                }}>{data && data.intro}</ReactMarkdown>
              </div>
            </div>

            <div className="m-auto px-10 sm:px-28 md:flex-row md:px-48 lg:w-3/4 lg:text-xl xl:w-3/5 xl:text-2xl lg:leading-5 xl:leading-7">
              <div className="px-10 pt-16 pb-12 text-xl leading-5 text-center mx-auto prose-sm shadow-xl bg-brand-kuarahy/80 text-brand-orange-6 lg:prose lg:scale-110">
                <ReactMarkdown components={{
                  a: ({ children, href }) => {
                    return (
                      <a href={href} target="_blank" rel="noreferrer">{children}</a>
                    )
                  }
                }}>{data && data.intro2}</ReactMarkdown>
              </div>
            </div>
          </div>
        </AnimatedTitleByScroll>
      </div>
    </header>
  )
}