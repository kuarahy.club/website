import { useEffect, useState } from 'react'
import {
  motion,
  useSpring,
  useTransform,
  useViewportScroll
} from "framer-motion"
import Link from 'next/link'
import { bh, vh, vw } from '../lib/animationHelpers'
import { fadeOutTemploStart, fadeOutTemploAndLogoEnd, fadeOutLogoStart } from '../config'
import useTranslation from '../lib/translations/useTranslation.hook'

const endPositionTransition = [
  [bh(fadeOutTemploAndLogoEnd), bh(fadeOutTemploAndLogoEnd)+1],
  [-3000, -3000]
]

// responsive config
const getAnimationConfig = (item, attr) => {
  const animatinoConfig = {
    logo: {
      spring: {
        stiffness: 150, damping: 40
      },
      height: [
          [0, bh(90)],
        [180, 100]
      ],
      position: [
            [0, bh(35), bh(fadeOutLogoStart), bh(fadeOutLogoStart)+1, ...endPositionTransition[0]]
        ,[vh(22), vh(10), vh(10), vh(10)-0.25, ...endPositionTransition[1]]
      ]
    },
    resplandorExt: {
      position: [
            [0, bh(35), bh(fadeOutLogoStart), bh(fadeOutLogoStart)+1, ...endPositionTransition[0]]
        ,[vh(35), vh(6), vh(6), vh(6)-0.25, ...endPositionTransition[1]]
      ]
    },
    resplandorInt: {
      position: [
              [0, bh(35), bh(fadeOutLogoStart), bh(fadeOutLogoStart)+1, ...endPositionTransition[0]]
        ,[vh(24), vh(6), vh(3), vh(3)-0.25, ...endPositionTransition[1]]
      ]
    }
  }
  if (typeof window === "undefined") { return animatinoConfig[item][attr] }
  
  // landscape ////////////////////////////////////////////
  // xs
  // sm
  if (window.innerHeight >= 450) {}
  // md
  if (window.innerHeight >= 530) {}
  // normal ////////////////////////////////////////////
  // xs
    // vegetacion más separado
    // vegetacion más alto al inicio
  // xs-sm
  if (window.innerWidth >= 450) {
    animatinoConfig.resplandorExt.position = [
            [0, bh(35), bh(fadeOutLogoStart), bh(fadeOutLogoStart)+1, ...endPositionTransition[0]]
      ,[vh(35), vh(2), vh(2), vh(2)-0.25, ...endPositionTransition[1]]
    ]
    animatinoConfig.resplandorInt.position = [
            [0, bh(35), bh(fadeOutLogoStart), bh(fadeOutLogoStart)+1, ...endPositionTransition[0]]
      ,[vh(21), vh(2), vh(1), vh(1)-0.25, ...endPositionTransition[1]]
    ]
  }
  // sm
  if (window.innerWidth >= 640) {}
  // md
  if (window.innerWidth >= 768) {
    animatinoConfig.resplandorInt.position = [
          [0, bh(35), bh(fadeOutLogoStart), bh(fadeOutLogoStart)+1, ...endPositionTransition[0]]
      ,[vh(19), vh(-1), vh(-2), vh(-2)-0.25, ...endPositionTransition[1]]
    ]
  }
  // lg
  if (window.innerWidth >= 1024) {}
  // xl
  if (window.innerWidth >= 1280) {
    animatinoConfig.resplandorInt.position = [
           [0, bh(35), bh(fadeOutLogoStart), bh(fadeOutLogoStart)+1, ...endPositionTransition[0]]
      ,[vh(21), vh(2), vh(1), vh(1)-0.25, ...endPositionTransition[1]]
    ]
  }
  // 2xl casi (oficialmente empieza en 1536)
  if (window.innerWidth >= 1400) {}

  return animatinoConfig[item][attr]
}

export default function EscenaHeaderLogo() {
  const { t, locale } = useTranslation()
  // Vertical scroll distance in pixels.
  const { scrollY } = useViewportScroll();
    
  // LOGO ///////////////////////////////////////////////
  const logo = {}
  logo.heightYRange = useTransform(scrollY, ...getAnimationConfig('logo', 'height'));
  logo.heightValue = useSpring(logo.heightYRange, getAnimationConfig('logo', 'spring'));
  // top
  logo.positionYRange = useTransform(scrollY, ...getAnimationConfig('logo', 'position'), {clamp: false})
  ///////////////////////////////////////////////////////

  // RESPLANDOR EXTENSO ///////////////////////////////////////////////
  const resplandorExt = {}
  // height
  resplandorExt.heightYRange = useTransform(scrollY, [0, bh(35)], [60, 90]);
  // top
  resplandorExt.positionYRange = useTransform(scrollY, ...getAnimationConfig('resplandorExt', 'position'), {clamp: false})
  // opacity noche
  resplandorExt.opacityNocheYRange = useTransform(scrollY, 
      [0, bh(15), bh(33), bh(fadeOutLogoStart), bh(fadeOutTemploAndLogoEnd), bh(fadeOutTemploAndLogoEnd)+1]
    ,[0, 0, 1, 1, 0, 0]
  ,{clamp: false})
  ///////////////////////////////////////////////////////

  // RESPLANDOR INTENSO ///////////////////////////////////////////////
    const resplandorInt = {}
    // height
    resplandorInt.heightYRange = useTransform(scrollY, [0, bh(35)], [80, 90]);
    // top
    resplandorInt.positionYRange = useTransform(scrollY, ...getAnimationConfig('resplandorInt', 'position'), {clamp: false})
    // opacity noche
    resplandorInt.opacityNocheYRange = useTransform(scrollY, 
        [0, bh(15), bh(33), bh(fadeOutLogoStart), bh(fadeOutTemploAndLogoEnd), bh(fadeOutTemploAndLogoEnd)+1]
      ,[0.4, 0.4, 1, 1, 0, 0]
    ,{clamp: false})
    ///////////////////////////////////////////////////////

  return (
    <>
      {/* resplandor extenso => opacity desde 0 */}
      <motion.div
        className="fixed"
        style={{ 
          y: resplandorExt.positionYRange,
          width: 2000,
          left: -1000
        }}
      > 
      <div style={{
        position: 'relative',
        width: '100%'
      }} className="flex place-content-center left-[48vw]">
        <motion.img
          src={`/assets/escena/xs/resplandor-extenso.png`} alt={t('common.site-title')}
          style={{
            position: 'relative',
            opacity: resplandorExt.opacityNocheYRange,
            top: -270,
            height: resplandorExt.heightYRange.get() + 'vh',
          }}
        />
      </div>
      </motion.div>

      {/* resplandor intenso => opacity desde 30 */}
      <motion.div
        className="fixed"
        style={{ 
          y: resplandorInt.positionYRange,
          width: 2000,
          left: -1000
        }}
      > 
      <div style={{
        position: 'relative',
        width: '100%'
      }} className="flex place-content-center left-[50vw] md:top-5 xl:-top-4">
        <motion.img
          src={`/assets/escena/xs/resplandor-intenso.png`} alt={t('common.site-title')}
          style={{
            position: 'relative',
            opacity: resplandorInt.opacityNocheYRange,
            top: -270,
            height: resplandorInt.heightYRange.get() + 'vh',
          }}
        />
      </div>
      </motion.div>

      {/* logo => achicar y subir */}
      <motion.div
        className="w-full fixed"
        style={{ 
          y: logo.positionYRange
        }}
      > 
        <Link href={`/${locale}`}>
          <h1 className="flex items-center mx-auto cursor-pointer place-content-center m-auto" alt={t('common.site-title')} title={t('common.site-title')}>
            <motion.img
              src={`/assets/escena/lg/logo.png`} alt={t('common.site-title')}
              style={{
                height: logo.heightValue
              }}
            />
          </h1>
        </Link>
      </motion.div>
    </>
  )
}