import { useRef } from 'react'
import _ from 'lodash'
import ReactMarkdown from 'react-markdown'
import Layout from '../../components/layout'
import HeaderMain from '../../components/headerMain'
import withLocale from '../../lib/translations/withLocale.hoc'
import config from '../../config'
import { readLocaleStrings } from '../../lib/translations/readLocaleStrings'
import useTranslation, { getPostContentByLocaledURL } from '../../lib/translations/useTranslation.hook'
import SectionFooter from '../../components/sectionFooter'
import HomeContent from '../../components/homeContent'
import useScrollSpy from 'react-use-scrollspy'
import map from 'lodash.map'
import ScrollDown from '../../components/scrollDown'


export function BlogPost(props) {
  const { t, locale } = useTranslation()
  const { portadaContent } = props
  const data = portadaContent;
  // scroll spy
  const sectionRefsObj = {
    logoHeader: {
      key: 'logoHeader', ref: useRef(null), 
      scroll: { duration: 3000, offsetDown: 0 } 
    },
    lemaHeader: {
      key: 'lemaHeader', ref: useRef(null), 
      scroll: { duration: 3000, offset: -300, smoothDown: 'easeOutQuad' } 
    },
    alertHeader: {
      key: 'alertHeader', ref: useRef(null), 
      scroll: {
        duration: 3000,
        offset: -215,
        smooth: true
      } 
    },
    propositoHeader: {
      key: 'propositoHeader', ref: useRef(null), 
      scroll: { duration: 3000, offset: -350, smooth: true } 
    },
    fundacionContent: { 
      key: 'fundacionContent', ref: useRef(null),
      scroll: { duration: 2000, offset: -130 } 
    },
    clubContent: { 
      key: 'clubContent', ref: useRef(null),
      scroll: { duration: 2000, offset: -110 } 
    },
    incubadoraContent: { 
      key: 'incubadoraContent', ref: useRef(null),
      scroll: { duration: 2000, offset: -130 } 
    },
    staffContent: { 
      key: 'staffContent', ref: useRef(null),
      scroll: { duration: 2000, offset: -130 } 
    },
    roadmapContent: { 
      key: 'roadmapContent', ref: useRef(null),
      scroll: { duration: 2000, offset: -130 } 
    },
    contactContent: { 
      key: 'contactContent', ref: useRef(null),
      scroll: { duration: 2000, offset: -300 } 
    },
  }
  const sectionRefsList = map(sectionRefsObj, 'ref')
  const activeSection = useScrollSpy({
    sectionElementRefs: sectionRefsList,
    offsetPx: -400,
  })

  return (
    <Layout home sectionRefsObj={sectionRefsObj}>
      <HeaderMain id="HeaderMain" data={data} sectionRefsObj={sectionRefsObj}></HeaderMain>
      <HomeContent data={data} sectionRefsObj={sectionRefsObj}></HomeContent>
      <SectionFooter id="footer" sectionRefsObj={sectionRefsObj}></SectionFooter>
      <ScrollDown activeSection={activeSection} sectionRefsObj={sectionRefsObj}></ScrollDown>
    </Layout>
  )
}

export async function getStaticPaths () {
  const paths = []
  config.locales.forEach(locale => {
    paths.push({ params: { lang: locale } })
  })
  return {
    paths, fallback: false
  }
}

export async function getStaticProps ( props ) {
  let { lang } = props.params
  return {
    props: {
      locale: lang,
      localeStrings: readLocaleStrings(lang, ['common', 'inicio']),
      portadaContent: getPostContentByLocaledURL('paginas', 'portada', lang)
    }
  }
}

export default withLocale(BlogPost)