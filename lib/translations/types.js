import config from '../../config'

export function isLocale(tested) {
  return config.locales.some(locale => locale === tested)
}