import React from 'react'
import { useRouter } from 'next/router'
import { isLocale } from './types'
import config from '../../config'

export const LocaleContext = React.createContext({
  strings: {}, // by locale and namespaces
  setStrings: () => null,
  locale: config.defaultLocale,
  setLocale: () => null
})

export const LocaleProvider = ({ lang, localeStrings, children }) => {
  const [locale, setLocale] = React.useState(lang)
  const [strings, setStrings] = React.useState(localeStrings)
  const { query } = useRouter()

  React.useEffect(() => {
    if (strings !== localeStrings) {
      setStrings(localeStrings)
    }
  }, [localeStrings])

  React.useEffect(() => {
    if (locale !== localStorage.getItem('locale')) {
      localStorage.setItem('locale', locale)
    }
  }, [locale])

  React.useEffect(() => {
    if (typeof query.lang === 'string' && isLocale(query.lang) && locale !== query.lang) {
      setLocale(query.lang)
    }
  }, [query.lang, locale])

  return <LocaleContext.Provider value={{ strings, setStrings, locale, setLocale }}>{children}</LocaleContext.Provider>
}