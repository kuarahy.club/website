export default {
  'paths': [{
    'en': 'what-is',
    'es': 'que-es'
  }, {
    'en': 'privacy-policy',
    'es': 'politica-de-privacidad'
  }, {
    'en': 'terms-and-conditions',
    'es': 'terminos-y-condiciones'
  }]
}