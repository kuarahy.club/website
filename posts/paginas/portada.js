export default {
////////////////////////////////
// en
////////////////////////////////
'title_en': "",
'lema_en': [
  'Un',
  'Revolucionario',
  'Estilo de',
  'Hacer Negocios',
  'con',
  'Responsabilidad',
  'SOCIAL',
],
'intro_en': `
Nuestro propósito es priorizar el ***impacto social*** mediante el poder de la comunidad y el desarrollo de nuevas soluciones tecnológicas en la Web3

*- la nueva era de Internet -*
`,
'intro2_es': `
  Estamos conformados por 3 partes, 
  una Fundación como razón de Ser,
  financiada por los queridos miembros
  del Club mediante innovadores proyectos
  generados por nuestra Incubadora de proyectos.
`,
'alertTitle_en': '¡Está cerca!',
'alertDesc_en': [
  'Colección',
  'REDENCIÓN',
  'Primer NFT Social del País',
  'PRÓXIMAMENTE'
],

'fundacionTitle_en': ['Una', 'Fundación', 'Como razón de Ser'],
'fundacionSubtitle_en': `El eje sobre el cual giran y apuntan todos los esfuerzos de Kuarahy`,
'fundacionContent_en': `
Entre el **30% y el 50% de los ingresos de *todos los proyectos*** están destinados a la fundación. 

Los primeros esfuerzos giran en torno a la concientización sobre la responsabilidad de la sociedad sobre el cuidado del medio ambiente mediante el ejemplo.
`,

'clubTitle_en': ['Un', 'Club', 'Para compartir'],
'clubSubtitle_en': `Un exclusivo grupo de personas conscientes del impacto de sus acciones`,
'clubContent_en': `
Se encargan de mantener brillando al propósito.

Entre los beneficios que estamos preparando se encuentran una memorable experiencia interactiva, accesos vip a eventos y exclusivas ofertas de artículos antes que los demás.
`,

'incubadoraTitle_en': ['Una', 'Incubadora', 'de Proyectos', 'Para generar Valor'],
'incubadoraSubtitle_en': `Nuestra fuente generadora de valor`,
'incubadoraContent_en': `
Las bases para aportar valor durante el largo plazo de manera colaborativa potenciando a la comunidad.

Gestionada por un equipo multidisciplinario con experiencia en la creación de proyectos tecnológicos y artísticos.
`,

'staffTitle_en': 'Miembros del Staff',

'roadmapTitle_en': 'Roadmap',
'roadmapDesc_en': `
Estamos preparando los primeros tres proyectos que aportarán:

Una experiencia memorable para los miembros del Club.

Más ingresos y renombre para empresas adheridas.

Las primeras acciones de la Fundación.

**Interés cultural y buena onda.**
`,
'roadmapItems_en': [{
  'title': 'Colección “Redención”',
  'alert': '¡Sale en pocas semanas!',
  'date': '',
  'content': 'Primer NFT social del Paraguay'
}, {
  'title': 'Segunda colección',
  'alert': '',
  'date': 'Próximamente',
  'content': ''
}, {
  'title': 'App de interacción social',
  'alert': '',
  'date': 'Próximamente',
  'content': ''
}],


////////////////////////////////
// es
////////////////////////////////
'title_es': "",
'lema_es': [
  'Un',
  'Revolucionario',
  'Estilo de',
  'Hacer Negocios',
  'con',
  'Responsabilidad',
  'SOCIAL',
],
'intro_es': `
Nuestro propósito es priorizar el ***impacto social*** mediante el poder de la comunidad y el desarrollo de nuevas soluciones tecnológicas en la Web3

*- la nueva era de Internet -*
`,
'intro2_es': `
  Estamos conformados por 3 partes, 
  una Fundación como razón de Ser,
  financiada por los queridos miembros
  del Club mediante innovadores proyectos
  generados por nuestra Incubadora de proyectos.
`,
'alertTitle_es': '¡Está cerca!',
'alertDesc_es': [
  'Colección',
  'REDENCIÓN',
  'Primer NFT Social del País',
  'PRÓXIMAMENTE'
],

'fundacionTitle_es': ['Una', 'Fundación', 'Como razón de Ser'],
'fundacionSubtitle_es': `El eje sobre el cual giran y apuntan todos los esfuerzos de Kuarahy`,
'fundacionContent_es': `
Entre el **30% y el 50% de los ingresos de *todos los proyectos*** están destinados a la fundación. 

Los primeros esfuerzos giran en torno a la concientización sobre la responsabilidad de la sociedad sobre el cuidado del medio ambiente mediante el ejemplo.
`,

'clubTitle_es': ['Un', 'Club', 'Para compartir'],
'clubSubtitle_es': `Un exclusivo grupo de personas conscientes del impacto de sus acciones`,
'clubContent_es': `
Se encargan de mantener brillando al propósito.

Entre los beneficios que estamos preparando se encuentran una memorable experiencia interactiva, accesos vip a eventos y exclusivas ofertas de artículos antes que los demás.
`,

'incubadoraTitle_es': ['Una', 'Incubadora', 'de Proyectos', 'Para generar Valor'],
'incubadoraSubtitle_es': `Nuestra fuente generadora de valor`,
'incubadoraContent_es': `
Las bases para aportar valor durante el largo plazo de manera colaborativa potenciando a la comunidad.

Gestionada por un equipo multidisciplinario con experiencia en la creación de proyectos tecnológicos y artísticos.
`,

'staffTitle_es': 'Miembros del Staff',

'roadmapTitle_es': 'Roadmap',
'roadmapDesc_es': `
Estamos preparando los primeros tres proyectos que aportarán:

Una experiencia memorable para los miembros del Club.

Más ingresos y renombre para empresas adheridas.

Las primeras acciones de la Fundación.

**Interés cultural y buena onda.**
`,
'roadmapItems_es': [{
  'title': 'Colección “Redención”',
  'alert': '¡Sale en pocas semanas!',
  'date': '',
  'content': 'Primer NFT social del Paraguay'
}, {
  'title': 'Segunda colección',
  'alert': '',
  'date': 'Próximamente',
  'content': ''
}, {
  'title': 'App de interacción social',
  'alert': '',
  'date': 'Próximamente',
  'content': ''
}],


////////////////////////////////
// common
////////////////////////////////
'miembros': [{
  'name': 'Guillermo Martínez',
  'tags': 'CEO, Blockchain Developer'
}, {
  'name': 'Isael Samir',
  'tags': 'COO, Community Manager'
}, {
  'name': 'José Acosta',
  'tags': 'Art Director, Brand Manager'
}, {
  'name': 'José Peters',
  'tags': 'Concept Developer, Senior Artist'
}, {
  'name': 'Matías Martínez',
  'tags': 'Web & Social Media Designer'
}],
    
};
  