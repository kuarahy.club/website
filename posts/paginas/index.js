export default [
  {
    'en': "home",
    'es': "portada",
    'mainImage': "image.png",
    'bgColor': "green-3",
    'titleColor': "text-white",
  },
  {
    'en': "galeria",
    'es': "galeria"
  }
];